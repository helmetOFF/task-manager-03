## PROJECT INFO

TASK MANAGER

## DEVELOPER INFO

**NAME:** VLADISLAV HALMETOV

**E-MAIL:** halmetoff@gmail.com

## SOFTWARE

- JDK 1.8
- MS WINDOWS 10

## PROGRAM RUN

```bash
java -jar ./task-manager.jar
```

## SCREENSHOTS

https://drive.google.com/file/d/1pFFZ1l0BbhSW_ytYCc1IfVg_Dme-lJfN/view?usp=sharing